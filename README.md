# Formation Docker

# Docker - TP1 : Images, conteneurs et volumes
> **Objectifs du TP** :
> - Se familiariser avec les concepts d’images et de conteneurs
> - Manipuler les commandes de base de Docker : Créer un conteneur et le manipuler
> - Comprendre le fonctionnement et la manipulation des volumes

# Docker - TP2 : Utilisation des images
> **Objectifs du TP** :
>- Comprendre comment utiliser les images sur Docker

# Docker - TP3 : Utilisation des conteneurs de manière autonome
> **Objectifs du TP** :
>- Autonomie sur l'utilisation des conteneurs

# Docker - TP4 : La création d'images
> **Objectifs du TP** :
>- Création d'images et utilisation de la Registry Docker Hub

# Docker - TP5 : Utilisation des networks de manière autonome
> **Objectifs du TP** :
>- Autonomie sur l'utilisation des networks

# Docker - TP6 : Construction d'images en Dockerfile
> **Objectifs du TP** :
>- Utiliser un registre afin de stocker des images Docker
>- Comprendre comment est construite une image Docker avec son application en Dockerfile

# Docker - TP6 BIS : Construction d'images Dockerfile avec Multistaging
> **Objectifs du TP** :
>- Manipuler la création d'images Docker avec son application en Dockerfile et en mode Multistaging

# Docker - TP7 : Utilisation de Docker-Compose
> **Objectifs du TP** :
>- Prise en main de l'outil docker-compose

# Docker - TP8 : Autonomie avec Docker-Compose
> **Objectifs du TP** :
>- Manipulation de l'outil docker-compose

# Docker - TP9 : Gestion des networks avec Docker-Compose
> **Objectifs du TP** :
>- Manipulation des networks avec docker-compose

# Docker - TP10 : Création d'un environnement Docker-Compose 
> **Objectifs du TP** :
>- Manipulation avancée de Docker-Compose


